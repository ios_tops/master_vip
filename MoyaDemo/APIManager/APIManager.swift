//
//  ViewController.swift
//  MoyaDemo
//
//  Created by Aiyub Munshi on 10/07/18.
//  Copyright © 2018 Aiyub Munshi. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJSON
import Moya


var apiLoadingTestTime = 0.0

//endPointClouser used for the additional header parameters
extension WBService
{
    static var endPointClouser: MoyaProvider<WBService>.EndpointClosure{
        return { (target: WBService) -> Endpoint<WBService> in
            var defaultEndpoint = MoyaProvider.defaultEndpointMapping(for: target)
            defaultEndpoint = defaultEndpoint.adding(newHTTPHeaderFields: ["Content-Type":"application/x-www-form-urlencoded"])
           // defaultEndpoint = defaultEndpoint.adding(newHTTPHeaderFields: APISecurity.security().dictionary())
           // defaultEndpoint = defaultEndpoint.adding(newHTTPHeaderFields: APISecurity.Authorization.authDictionary)
            return defaultEndpoint
        }
    }
}

enum WBService
{
    case getUserListing()
}

// MARK: - TargetType Protocol Implementation
extension WBService : TargetType 
{
    var headers: [String : String]? {
        
        var headers : [String : String] = [:]
        
        switch self
        {
        case .getUserListing:
            break
        default:
            headers = APISecurity.Authorization.authDictionary
        }
        
        headers += APISecurity.security().dictionary()
        
        return headers
    }
    
    
    // MARK: - baseURL
    var baseURL: URL {        
//        return URL(string: "")!; //local
        return URL(string: "http://www.mocky.io/v2/")! // Live
    }
    
    // MARK: - path
    var path: String
    {
        switch self
        {
        case .getUserListing:
            return "5b57035d31000044214d205d"
        }
    }
    
    // MARK: - method
    var method: Moya.Method {
        switch self
        {
        case .getUserListing :
            return .get
        }
    }
    
    // MARK: - parameterEncoding
    var parameterEncoding: ParameterEncoding
    {
        switch self {
        case .getUserListing:
            return URLEncoding.default
        }
    }
    
    // MARK: - task
    var task: Task {
        switch self
        {
        case .getUserListing :
            return .requestParameters(parameters: self.parameters!, encoding: parameterEncoding)
     
//        case .getSongsAllAlbum, .getHomePageSlider, .getAllCategory, .subscription, .notificationList, .signOut:
//            return .requestPlain
//
//        case .editProfileNew:
//            return .uploadMultipart(self.multipartBody!)
//
        }
    }
    
//    var multipartBody :[Moya.MultipartFormData]?{
//        switch self
//        {
//        case .editProfileNew(let firstName, let lastName, let email, let mobile, let profilePic):
//
//            let vFirstData = Moya.MultipartFormData(provider: MultipartFormData.FormDataProvider.data(firstName.data(using: .utf8)!), name: requestParameter.firstName)
//            let vLastData = Moya.MultipartFormData(provider: MultipartFormData.FormDataProvider.data(lastName.data(using: .utf8)!), name: requestParameter.lastName)
//            let vEmailData = Moya.MultipartFormData(provider: MultipartFormData.FormDataProvider.data(email.data(using: .utf8)!), name: requestParameter.emailId)
//            let vMobileData = Moya.MultipartFormData(provider: MultipartFormData.FormDataProvider.data(mobile.data(using: .utf8)!), name: requestParameter.mobileNumber)
//
//            guard let data = UIImageJPEGRepresentation(profilePic, 0.5) else { return nil }
//
//            return [Moya.MultipartFormData(provider: MultipartFormData.FormDataProvider.data(data), name: requestParameter.profilePic, fileName: "profileImage.jpg", mimeType: "image/png"),vFirstData,vLastData,vEmailData,vMobileData]
//        default:
//            return []
//        }
//    }
    
    // MARK: - parameters
    var parameters: [String: Any]?
    {
        var params : [String: Any] = [:]
        switch self
        {
            
        case .getUserListing():
            break
            
        }
        
        params += APISecurity.security().dictionary()
        print("TDService := \(self)");
        print("params := \(params)");
        
        return params
    }
    
    var sampleData: Data {
        switch self {
        default:
            return Data()
        }
    }
}

// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return self.data(using: .utf8)!
    }
}

func += <Key, Value> ( left: inout [Key:Value], right: [Key:Value]) {
    for (key, value) in right {
        left.updateValue(value, forKey: key)
    }
}
