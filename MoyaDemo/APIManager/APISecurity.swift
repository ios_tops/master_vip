//
//  ViewController.swift
//  MoyaDemo
//
//  Created by Aiyub Munshi on 10/07/18.
//  Copyright © 2018 Aiyub Munshi. All rights reserved.
//


import Foundation
import Alamofire
import Moya



class APISecurity: NSObject {
    
    struct Authorization {
        static var key: String? {
            set{
                guard let unwrappedKey = newValue else{
                    return
                }
                UserDefaults.standard.set(unwrappedKey, forKey: Constant.UserDefaultKey.kToken)
                UserDefaults.standard.synchronize()
            }get{
                return UserDefaults.standard.value(forKey: Constant.UserDefaultKey.kToken) as? String
            }
        }
        
        
        static func removeAuthKey(){
            if UserDefaults.standard.value(forKey: Constant.UserDefaultKey.kToken) != nil {
                UserDefaults.standard.removeObject(forKey: Constant.UserDefaultKey.kToken)
                UserDefaults.standard.synchronize()
            }
        }
       
        static func removeKeys(){
            self.removeAuthKey()
        }
        
        static var authDictionary: [String:String] {
            var auth : [String:String] = [:]
            if let authKey = APISecurity.Authorization.key
            {
                auth["vToken"] = authKey
            }
            return auth;
        }
    }
    
    class func security() -> APISecurity
    {
        let secureObject = APISecurity()
        return secureObject;
    }
    
    func dictionary() -> Dictionary<String,String>
    {
        var dictSecurity : Dictionary<String,String> = [:]
        
//        dictSecurity["token"] = self.token;
//        dictSecurity["timestamp"] = "\(self.timestamp)";
//        dictSecurity["nonce"] = self.nonce;
        dictSecurity["Content-Type"] = "application/x-www-form-urlencoded"
        
        
        return dictSecurity;
    }
    
    //this is added here to remove the dependency of extensions, so any one can use by using this single file

}

