//
//  SOIntExtension.swift
//  Aiyub Munshi
//
//  Created by Aiyub Munshi216 on 21/06/16.
//  Copyright © 2016 Aiyub Munshi. All rights reserved.
//

import Foundation
import UIKit

extension Bool {
    var intValue: Int {
        return self ? 1 : 0
    }
}
extension CGFloat{ 
    var fontSize : CGFloat {
        var deltaSize : CGFloat = 0;
        switch (UIDevice.deviceType) {
        case .iPhone4_4s,
             .iPhone5_5s :
            deltaSize = -1;
        case .iPhone6_6s :
            deltaSize = 2;
        case .iPhone6p_6ps :
            deltaSize = 2;
        case .after_iPhone6p_6ps :
            deltaSize = 3;
        case .iPad :
            deltaSize = 5;
        default:
            deltaSize = 0;
        }
        let selfValue = self;
        return CGFloat(selfValue) + deltaSize;
    }
}
