//
//  SOUIFontExtension.swift
//  Aiyub Munshi
//
//  Created by Aiyub Munshi216 on 21/06/16.
//  Copyright © 2016 Aiyub Munshi. All rights reserved.
//

import Foundation
import UIKit



extension UIFont {
    static var delatFontSize : CGFloat {
        return 0.0
    }
    class func font_title(_ size : CGFloat) -> UIFont {
        return UIFont(name: "RobotoSlab-Bold", size: size)!;
    }
    class func font_Slab_regular(_ size : CGFloat) -> UIFont {
        return UIFont(name: "RobotoSlab-Regular", size: size)!;
    }
    class func font_regular(_ size : CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Regular", size: size)!;
    }
    class func font_bold(_ size : CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Bold", size: size)!;
    }
    class func font_thin(_ size : CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Thin", size: size)!;
    }
    class func font_light(_ size : CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Light", size: size)!;
    }
    class func font_medium(_ size : CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Medium", size: size)!;
    }
    class func font_italic(_ size : CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Italic", size: size)!;
    }
    class func printFonts() {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName)
            print("--- Font Names = [\(names)]")
        }
    }
}

extension UILabel
{
    open override func awakeFromNib()
    {
        super.awakeFromNib()
        self.font = self.font.withSize(self.font.pointSize.fontSize)
    }
}

extension UIButton
{
    open override func awakeFromNib()
    {
        super.awakeFromNib()
        self.titleLabel?.font = self.titleLabel?.font.withSize((self.titleLabel?.font.pointSize.fontSize)!)
    }
}

extension UITextField
{
    open override func awakeFromNib()
    {
        super.awakeFromNib()
        self.font = self.font?.withSize((self.font?.pointSize.fontSize)!)
    }
}
extension UITextView
{
    open override func awakeFromNib()
    {
        super.awakeFromNib()
        self.font = self.font?.withSize((self.font?.pointSize.fontSize)!)
    }
    
}
