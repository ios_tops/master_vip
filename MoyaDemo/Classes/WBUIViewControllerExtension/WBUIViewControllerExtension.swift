//
//  WBUIViewControllerExtension.swift
//  Aiyub Munshi
//
//  Created by Aiyub Munshi021 on 09/11/17.
//  Copyright © 2017 Aiyub Munshi021. All rights reserved.
//

import Foundation
import UIKit


func delay(_ delay:Double, closure:@escaping ()->()) {
    let when = DispatchTime.now() + delay
    DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
}

extension UIViewController
{
    func showAlertAction(_ preferredStyle : UIAlertControllerStyle
        , title : String? = nil
        , message : String? = nil
        , buttons : [WBAlertActionType] = [WBAlertActionType("OK")]
        , sourceView : Any? = nil
        ,withCompletion block : ((Int) -> Void)? = nil) {
        
        let alert : UIAlertController = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        
        for button in buttons {
            
            let action : UIAlertAction = UIAlertAction(title: button.title, style: button.type, handler: { (action : UIAlertAction) in
                
                let index = buttons.index{$0.title == button.title}
                block?(index!)
                
            })
            
            alert.addAction(action)
        }
        
        switch preferredStyle {
        case .alert:
            self.present(alert, animated: true, completion: nil)
            break
        case .actionSheet:
            self.present(alert, from: sourceView ?? self.view)
            break
        }
    }
    
    func present(_ alertController: UIAlertController, from source: Any?){
        
        if (UIDevice.deviceType == .iPad)
        {
            let popPresenter = alertController.popoverPresentationController;
            if source is UIBarButtonItem
            {
                popPresenter?.barButtonItem = source as? UIBarButtonItem
                popPresenter?.sourceView = self.view
                popPresenter?.sourceRect = self.view.bounds
            }
            else
            {
                popPresenter?.sourceView = source as? UIView
                popPresenter?.sourceRect = ((source as? UIView)?.frame)!
            }
            self.present(alertController, animated: true, completion: nil);
        }
        else
        {
            self.present(alertController, animated: true, completion: nil);
        }
    }
    
    var defaultMusicEdge : UIEdgeInsets{
        return UIEdgeInsets.zero
    }

}

extension UInt {
    var intValue : Int{
        return Int(self)
    }
}

//MARK: - Alert Button Type
struct WBAlertActionType {
    
    var title : String
    var type : UIAlertActionStyle
    
    init() {
        self.title = ""
        self.type = .default
    }
    
    init(_ title : String ,type : UIAlertActionStyle = .default) {
        
        self.title = title
        self.type = type
    }
}

extension String {
    
    func containsWhiteSpace() -> Bool {
        let range = self.rangeOfCharacter(from: CharacterSet.whitespaces)
        
        if let _ = range
        {
            return true
        }
        else
        {
            return false
        }
    }
}
extension UIView
{
    func makeRoundView(_ borderColor : UIColor, borderWidth : CGFloat, cornerRadius : CGFloat)
    {
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
    }

}

extension UIImageView
{
    func makeRoundImageView(_ borderColor : UIColor, borderWidth : CGFloat)
    {
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.cornerRadius = self.frame.size.height/2;
        self.layer.masksToBounds = true
    }
}

extension UIButton
{
    func changeButtonView(_ borderColor : UIColor, borderWidth : CGFloat, cornerRadius : CGFloat)
    {
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
    }
}

public extension NSObject{
    public class var myClassName: String {
        return String(describing: self)
    }
    
    public var myClassName: String {
        return String(describing: self)
    }
}

extension UISearchBar {
    public func setSerchTextcolorFont(color: UIColor, font : UIFont)
    {
        let clrChange = subviews.flatMap { $0.subviews }
        guard let sc = (clrChange.filter { $0 is UITextField }).first as? UITextField else { return }
        sc.textColor = color
        sc.font = font
    }
}

extension String
{
    var containsEmoji: Bool {
        for scalar in unicodeScalars {
            switch scalar.value {
            case 0x1F600...0x1F64F, // Emoticons
            0x1F300...0x1F5FF, // Misc Symbols and Pictographs
            0x1F680...0x1F6FF, // Transport and Map
            0x2600...0x26FF,   // Misc symbols
            0x2700...0x27BF,   // Dingbats
            0xFE00...0xFE0F,   // Variation Selectors
            0x1F900...0x1F9FF, // Supplemental Symbols and Pictographs
            0x1F1E6...0x1F1FF: // Flags
                return true
            default:
                continue
            }
        }
        return false
    }
    
    func stringIsEmpty(_ text : String) -> Bool
    {
        if text.trimmingCharacters(in: .whitespaces).isEmpty
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func nsRange(from range: Range<Index>) -> NSRange {
        return NSRange(range, in: self)
    }
    
    var isPhoneNumber: Bool
    {
        if isAllDigit
        {
            if self.count < 8 || self.count > 12
            {
                return false
            }
            return true
        }
        else
        {
            return false
        }
    }
    
    var isCountryCode: Bool
    {
        if isAllDigit
        {
            if self.count < 1 || self.count > 6
            {
                return false
            }
            return true
        }
        else
        {
            return false
        }
    }
    
    var isCharacter : Bool
    {
        let stricterFilterString = "[A-Za-z]+"
        let fieldString = NSPredicate(format: "SELF MATCHES %@", stricterFilterString)
        return fieldString.evaluate(with: self)
    }
  
    var isValidEmail : Bool
    {
        let stricterFilterString = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", stricterFilterString)
        let isValid = self.contains("..")
        return emailTest.evaluate(with: self) && !isValid
    }
    
    var isAllDigit : Bool
    {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = self.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  self == filtered
    }
}

extension MutableCollection {
    /// Shuffles the contents of this collection.
    mutating func shuffle() {
        let c = count
        guard c > 1 else { return }
        
        for (firstUnshuffled, unshuffledCount) in zip(indices, stride(from: c, to: 1, by: -1)) {
            let d: IndexDistance = numericCast(arc4random_uniform(numericCast(unshuffledCount)))
            let i = index(firstUnshuffled, offsetBy: d)
            swapAt(firstUnshuffled, i)
        }
    }
}
