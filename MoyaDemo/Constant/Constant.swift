//
//  Constant.swift
//  MyApp
//
//  Copyright © 2016 Aiyub Munshi. All rights reserved.
//

import Foundation
import UIKit
//Appdelegate shared object

let AppDelObj : AppDelegate = AppDelegate().sharedInstance()


class Constant: NSObject
{
    struct UserDefaultKey
    {
        static var kToken: String = "vToken"
        static var kLoginTokenKey: String = "com.sp.APISecurity.Authorization.loginTokenKey"
        static var currentUser: String = "currentUser"
        static var filterData: String = "allFilterData"
    }
}
