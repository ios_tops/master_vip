
import Foundation
import UIKit
import SwiftyJSON
import Moya
import Moya_SwiftyJSONMapper


public protocol DSSwiftyJSONAble : ALSwiftyJSONAble {
    init()
    var jsonDict:Dictionary<String,JSON>? {get set}
}

class BaseDM: DSSwiftyJSONAble {
    
    public var jsonArray:Array<JSON>?;
    public var jsonDict:Dictionary<String,JSON>?
    
    var responseCode : String = ""
    var responseMessage : String = ""
    var totalCount : Int = 0
    var pageNumber : Int = 0
    var aryString = [String]()
    var responseAuthentication : Int = 0

    var isInValidUser : Bool {
        return self.responseCode == "false"
    }

    var isSuccess : Bool {
        return self.responseCode == "true"
    }
    
//    var isAuthenticateUser : Bool {
//        return self.responseAuthentication == 1
//    }
    
    
    required init() {
    }
    
    required init?(jsonData:JSON){
        
//        QL1("BaseDM jsonData := \(jsonData)")
        self.jsonDict = jsonData["data"].dictionary
        self.jsonArray = jsonData["data"].array
        self.responseCode = jsonData["status"].stringValue
        self.responseMessage = jsonData["msg"].stringValue
//        self.totalCount = jsonData["response_total"].intValue
//        self.responseAuthentication = jsonData["response_authentication"].intValue

        if let dict = self.jsonDict {
            print(dict)
            
        }
        
        if let arr = self.jsonArray {
            print(arr)
        }
    }
}

extension BaseDM {
    /// Maps data received from the signal into an object which implements the HSSwiftyJSONAble protocol.
    /// If the conversion fails, the signal errors.
    public func map<DMType: DSSwiftyJSONAble>(to type:DMType.Type) -> DMType {
        
        guard var mappedObject = DMType(jsonData: JSON(self.jsonDict!)) else {
            return DMType()
        }
        mappedObject.jsonDict = self.jsonDict
        return mappedObject
    }
    
    /// Maps data received from the signal into an array of objects which implement the HSSwiftyJSONAble protocol
    /// If the conversion fails, the signal errors.
    public func map<DMType: DSSwiftyJSONAble>(to type:[DMType.Type]) -> [DMType] {
        
        guard let mappedArray = self.jsonArray else {
            return []
        }
        let mappedObjectsArray = mappedArray.flatMap { (json) -> DMType in
            var mappedObject = DMType(jsonData: json)
            mappedObject?.jsonDict = self.jsonDict
            return mappedObject!
        }
        
        return mappedObjectsArray
    }
}
