//
//	UserDM.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class UserDM : BaseDM{

	var personId : String!
	var personName : String!


    required init() {
        super.init()
    }
    
    required init?(jsonData:JSON){
        
        super.init(jsonData: jsonData)
        
        let json = jsonData
        if json.isEmpty{
            return
        }
		personId = json["person_id"].stringValue
		personName = json["person_name"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if personId != nil{
			dictionary["person_id"] = personId
		}
		if personName != nil{
			dictionary["person_name"] = personName
		}
		return dictionary
	}

}
