//
//  UserListInteractor.swift
//  MoyaDemo
//
//  Created by Aiyub Munshi on 10/07/18.
//  Copyright © 2018 Aiyub Munshi. All rights reserved.
//

import UIKit
import Moya

protocol UserListInteractorProtocol
{
    func callUserListAPI()
}

class UserListInteractor : UserListInteractorProtocol
{
    var presenter : UserListPresenterProtocol!
    
    
    func callUserListAPI()
    {
        let provider = MoyaProvider<WBService>(endpointClosure : WBService.endPointClouser)
        
        provider.request(WBService.getUserListing()) { (result) in
            
            switch result{
            case let .success(response):
                do {
                    let baseModel = try response.map(to: BaseDM.self)

                    guard baseModel.isSuccess else{
                        self.presenter.userListAPIFailure(message: baseModel.responseMessage)
                        return
                    }
                    
                    let userData = baseModel.map(to: [UserDM.self])
                    self.presenter.userListAPISuccess(userData: userData)
                }
                catch
                {
                    
                }
                break
                
            case let .failure(error):
                print(error.localizedDescription)
                self.presenter.userListAPIFailure(message: error.localizedDescription)

                break
            }
        }
    }
}
