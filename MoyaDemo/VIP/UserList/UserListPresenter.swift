//
//  UserListPresenter.swift
//  MoyaDemo
//
//  Created by Aiyub Munshi on 10/07/18.
//  Copyright © 2018 Aiyub Munshi. All rights reserved.
//

import UIKit

protocol UserListPresenterProtocol
{
    var userListData : [UserDM]? {get}
    
    func callAPI()
    func userListAPISuccess(userData : [UserDM])
    func userListAPIFailure(message : String)
}

class UserListPresenter : UserListPresenterProtocol
{
    var interactor : UserListInteractorProtocol!
    var viewcontroller : UserProtocol!
    
    var userListData : [UserDM]? = []
    
    //MARK:-
    func callAPI()
    {
        self.interactor.callUserListAPI()
    }
    
    func userListAPISuccess(userData : [UserDM])
    {
        self.userListData = userData
        self.viewcontroller.userListSuccess(true, message: "")
    }
    
    func userListAPIFailure(message: String)
    {
        self.viewcontroller.userListSuccess(false, message: message)
    }
    
}
