//
//  UserListVC.swift
//  MoyaDemo
//
//  Created by Aiyub Munshi on 10/07/18.
//  Copyright © 2018 Aiyub Munshi. All rights reserved.
//

import UIKit

//MARK:- Protocol and Protocol Method

protocol UserProtocol
{
    func userListSuccess(_ isSuccess : Bool, message : String)
}

extension UserListVC : UserProtocol
{
    func userListSuccess(_ isSuccess: Bool, message: String)
    {
        if isSuccess
        {
            self.userTableView.delegate = self
            self.userTableView.dataSource = self
            self.userTableView.reloadData()
        }
        else
        {
            self.showAlertAction(.alert, title: "Warning", message: message)
        }
    }
}

//MARK:-
class UserListVC: UIViewController
{
    var presenter : UserListPresenterProtocol!
    @IBOutlet var userTableView : UITableView!
    
    
    //MARK:-
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setUpVIPER()
        
        self.presenter.callAPI()
    }

    private func setUpVIPER()
    {
        let presenter = UserListPresenter()
        let interactor = UserListInteractor()
        
        self.presenter = presenter
        
        presenter.interactor = interactor
        presenter.viewcontroller = self
        
        interactor.presenter = presenter
    }
    
    //MARK:-
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func nameAndAddress(name:String, address:String?)
    {
        print(name)
    }
}

//MARK:- UITableView Delegate and DataSource

extension UserListVC : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.presenter.userListData!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : userCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! userCell
        cell.userData = self.presenter.userListData![indexPath.row]
        return cell
    }
}

//MARK:- UITableViewCell Class

class userCell: UITableViewCell {
    
    @IBOutlet var lblName : UILabel!
    
    var userData : UserDM!{
        didSet{
            self.lblName.text = userData.personName
        }
    }
    
}

extension String {
    
    func fromBase64() -> String {
        guard let data = Data(base64Encoded: self, options: Data.Base64DecodingOptions.init(rawValue: 0)) else {
            return self
        }
        return String(data: data, encoding: .utf8) ?? self
    }
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
}
