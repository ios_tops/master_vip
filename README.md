— List DEMO (VIP Structured Architecture)



This demo is having a simple list of the people. In this demo, We have maintained the VIP structure



-VIP (View , Interactor, Presenter) - Implementation.



-ViewController is performing operations and fill UI with data of the connected object of Presenter.



-The presenter is providing objects and methods to interactor which are used by interactor to fill the model.



-Model is created inside the Model Group and which is constructive.



-Finally, this filled model object of Presenter is used as data to fill the views of the viewController.



-In this way, this nested and customized structure performs efficiently and provide good functionality and code reusability to the Application.





Other Configurations :


- In this demo, Alamofire is used to call the API. swiftyJSON is used to parse the JSON.



- For more flexibility and global mapping, we are using MOYA along with alamofire.



- The configuration of MOYA is given in APIManager and API security File inside the APIManager group.









